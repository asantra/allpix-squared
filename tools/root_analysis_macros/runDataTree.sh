#! /bin/bash

#### run: bash makeDataTree.sh <input> <output> <detector>
#### using the local copy of allpix
echo "Installing AllPix>>>>>>"
source /srv01/agrp/arkas/arkaAllPix/build/setup/setup.sh

export LD_LIBRARY_PATH=/srv01/agrp/arkas/arkaAllPix/allpix-squared/lib:$LD_LIBRARY_PATH

inDirectory=${parname1}
inFileName=${parname2}
outDirectory=${parname3}
outFileName=${parname4}
detectorName=${parname5}
eventNeeded=${parname6}
signalName=${parname7}

echo "The input "${inDirectory}"/"${inFileName}" , the output "${outDirectory}"/"${outFileName}" for detector "${detectorName}" for event "${eventNeeded}" for sample "${signalName}
root -l ${inDirectory}/${inFileName} << EOF
.L /srv01/agrp/arkas/arkaAllPix/allpix-squared/lib/libAllpixObjects.so
.L /srv01/agrp/arkas/arkaAllPix/allpix-squared/tools/root_analysis_macros/ntuplizer.C++
auto outfile = new TFile("$outFileName", "RECREATE")
ntuplizer(_file0, "${detectorName}", ${eventNeeded},"${signalName}")
EOF

