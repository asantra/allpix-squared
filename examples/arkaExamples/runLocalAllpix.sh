#! /bin/bash
##
for thres in 120 40 200 300 400 500 700; do
  echo "----- RUNNING ON THRESHOLD: "${thres} " e -----"
  cp tutorial-simulationLOCAL.conf tutorial-simulationThreshold${thres}e.conf
  sed -i -e "s/ZZZ/${thres}/g" tutorial-simulationThreshold${thres}e.conf
  cd ../.. && bin/allpix -c examples/arkaExamples/tutorial-simulationThreshold${thres}e.conf && cd examples/arkaExamples
  mv /Users/arkasantra/AllPix2/allpix-squared/output/modules.root /Users/arkasantra/AllPix2/allpix-squared/output/modules_TrackerLayer1_Threshold${thres}e.root
  mv /Users/arkasantra/AllPix2/allpix-squared/output/data.root /Users/arkasantra/AllPix2/allpix-squared/output/data_TrackerLayer1_Threshold${thres}e.root
  rm *.conf-e
done
  
