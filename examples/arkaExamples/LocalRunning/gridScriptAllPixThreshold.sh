#! /bin/bash

#### script that run the python script, the MadGraph generator
# echo "Installing AllPix>>>>>>"
# source /cvmfs/clicdp.cern.ch/software/allpix-squared/latest/x86_64-centos7-gcc11-opt/setup.sh

### here -v is the version, which should be a unique integer for one series of runs.
echo "running the allpix script>>>>>>"
threshold=${1}
directory=${2}
iteration=${3}
depth=${4}
integrationTime=${5}
normalization=${6}
topdirectory=${7}

outRootFile="data_TrackerLayer1_CustomElectricField_Threshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Iteration${iteration}.root"
#### go to the directory where the files live
cd ${directory}
echo "I am now in "${PWD}

cp tutorial-simulationMASTER.conf tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf
sed -i -e "s|ZZZ|${threshold}|g" tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf
sed -i -e "s|XXX|${iteration}|g" tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf
sed -i -e "s|DDD|${depth}|g" tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf
sed -i -e "s|NNN|${normalization}|g" tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf
sed -i -e "s|III|${integrationTime}|g" tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf
sed -i -e "s/RRRR/${outRootFile}/g" tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf

/Users/arkasantra/AllPix2/allpix-squared/bin/allpix -c tutorial-simulationThreshold${threshold}e_Depth${depth}um_IntegrationTime${integrationTime}ns_Normalization${normalization}Vpercm_Iteration${iteration}.conf

cd ${topdirectory}
