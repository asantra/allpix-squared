#! /bin/bash


### how many jobs you want to submit, if -1, then submits 1 jobs
nJobs=${1}
depth=25
integrationTime=20
iteration=1
#### runid for the output job name in the grid, increased by 1 for each job
runid=0

### submitting 50 jobs or upto nJobs you wanted
for thresholdVal in 120; do ### 40 120 200 300 400 450 
    ### submit the normalization you want
    for normalizationVal in 1; do
    #for normalizationVal in 25; do
        for iter in $(seq 1 1 ${iteration}); do
            ### a counter value
            b=1
            ### runId increased by one
            runid=$(( $runid + $b ))
            
#             nThreshold=9
#             ### if number of jobs required is reached then break the loop
#             if [[ $runid -lt $nThreshold ]]; then
#                 continue
#             fi
            
            
            echo " normalization "$normalizationVal" V/cm threshold value: "$thresholdVal" runid: "$runid" iter: "$iter
            ### the bash script living
            TOPDIRECTORY="/Users/arkasantra/AllPix2/allpix-squared/examples/arkaExamples/LocalRunning"
            ### the place where the output and error file of the grid will live
            DESTINATION="/Volumes/Study/Weizmann_PostDoc/AllPix2Study/ThresholdRunningFiles"
            ### create the main directory if it does not exists
            mkdir -p ${DESTINATION}
            
            ### if main directory/run_id exists, delete
            if [[ -d "${DESTINATION}/run_$runid" ]]; then
                echo "Found a directory with output ${DESTINATION}/run_$runid! Deleting the previous one."
                rm -rf ${DESTINATION}/run_$runid
            fi

            #### create the run directory
            mkdir -p ${DESTINATION}"/run_"$runid"/"
            #### from where you are submitting jobs
            PRESENTDIRECTORY=${DESTINATION}"/run_"$runid"/"
            
            cp *conf ${PRESENTDIRECTORY}
            #### submit jobs to the PBS system
            source gridScriptAllPixThreshold.sh ${thresholdVal} ${PRESENTDIRECTORY} ${runid} ${depth} ${integrationTime} ${normalizationVal} ${TOPDIRECTORY}
            ### sleep for 1 s, so that there is no problem in submitting jobs to the grid
            sleep 1
            ### if number of jobs required is reached then break the loop
            if [[ $runid -eq $nJobs ]]; then
                break
            fi
        done
        ### if number of jobs required is reached then break the loop
        if [[ $runid -eq $nJobs ]]; then
            break
        fi
    done
    ### if number of jobs required is reached then break the loop
    if [[ $runid -eq $nJobs ]]; then
        break
    fi
done
